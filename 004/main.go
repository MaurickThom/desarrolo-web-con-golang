package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func main() {
	link := createURL()

	fmt.Println("El url es : " + link)

	request, err := http.NewRequest("GET", link, nil)

	if err != nil {
		panic(err)
	}
	request.Header.Set("HEADER", "value")
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	fmt.Println("El header es ", response.Header)
	fmt.Println("El body es ", response.Body)
	fmt.Println("El Status ", response.Status)

	body, err := ioutil.ReadAll(response.Body)

	if err != nil {
		panic(err)
	}
	fmt.Println("El body string(body) ", string(body))

}

func createURL() string {
	endPoint, err := url.Parse("/params")
	// endPoint, err := url.Parse("http://localhost:3000/params?name=thom")
	if err != nil {
		panic(err)
	}
	endPoint.Host = "localhost:3000"
	endPoint.Scheme = "http"

	query := endPoint.Query()
	query.Add("name", "Thom")

	endPoint.RawQuery = query.Encode()
	endPoint.RawQuery = query.Encode()
	return endPoint.String()
}
