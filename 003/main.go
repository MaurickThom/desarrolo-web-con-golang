package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

func main() {

	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		http.Redirect(res, req, "/params", http.StatusMovedPermanently)
	})

	http.HandleFunc("/params", func(w http.ResponseWriter, req *http.Request) {

		fmt.Println(req.Header)
		accessToken := req.Header.Get("access_token")
		if len(accessToken) != 0 {
			fmt.Println(accessToken)
		}
		req.Header.Set("name", "value")
		fmt.Println(req.Header)
		// fmt.Println(req.URL.RawQuery)
		// fmt.Println(req.URL.Query())             // Map
		// fmt.Println(req.URL.Query()["username"]) // Map
		// query := req.URL.Query()
		// name := query.Get("name")
		// query.Add("password", "thomabc123")
		// query.Del("username")

		// req.URL.RawQuery = query.Encode()
		// fmt.Println(req.URL) // Map

		// fmt.Fprintf(res, name, 200)
		query := req.URL.Query()
		filters, present := query["username"] //filters=["color", "price", "brand"]
		if !present || len(filters) == 0 {
			fmt.Println("filters not present")
		}
		w.WriteHeader(200)
		w.Write([]byte(strings.Join(filters, ",")))
	})

	log.Fatal(http.ListenAndServe(":3000", nil))
}
