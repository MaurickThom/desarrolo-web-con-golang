package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func YourHandler(w http.ResponseWriter, r *http.Request) {
	var params = mux.Vars(r)

	name := params["name"]
	id := params["id"]
	// w.Write([]byte("Gorilla!\n"))
	fmt.Fprintf(w, "Los parametros son "+name+" "+id)
}

func main() {
	r := mux.NewRouter()
	// Routes consist of a path and a handler function.
	r.HandleFunc("/user/{name}/{id:[0-9]+}", YourHandler).Methods("PUT", "DELETE")
	// express /user/:name/:id
	/*
		app.get('/:id(\\d+)/', function (req, res){
			// 	req.params.id is now defined here for you
		});
	*/
	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(":8000", r))
}
