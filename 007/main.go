package main

import (
	"fmt"
	"log"
	"net/http"
)

type User struct {
	name string
}

// esto se convierte en un handler
func (this *User) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hola "+this.name)
}

func main() {
	thom := &User{
		name: "thom",
	}
	mux := http.NewServeMux()
	// mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	// })
	mux.Handle("/thom", thom)

	server := &http.Server{
		Addr: "localhost:3000",
		// Handler: thom,
		Handler: mux,
	}
	log.Fatal(server.ListenAndServe())

}
