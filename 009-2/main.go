package main

import (
	"fmt"
	"log"
	"net/http"

	"./mux"
)

type User struct {
	name string
}

func (this *User) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	str := fmt.Sprintf("%v", this)
	fmt.Fprintf(w, str)
}

func main() {

	mux := mux.CreateMux()
	user := &User{
		name: "thom",
	}
	mux.AddMuxFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "hello")
	})

	mux.AddMuxFunc("/world", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "world")
	})

	mux.AddMuxHandler("/struct", user)

	// log.Fatal(http.ListenAndServe("localhost:3000", nil)) // nil deaultMuxServer
	log.Fatal(http.ListenAndServe("localhost:3000", mux)) // mux => un listado de rutas que tienen asociados funciones
}
