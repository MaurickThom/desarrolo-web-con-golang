package main

import (
	"log"
	"net/http"
	"text/template"
)

// var templates = template.Must(template.New("T").ParseFiles("templates/index.html", "templates/header.html", "templates/footer.html"))
var templates = template.Must(template.ParseGlob(`templates/**/*.html`))

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		err := templates.ExecuteTemplate(w, "Index", nil)
		if err != nil {
			panic(err)
		}
	})
	log.Fatal(http.ListenAndServe(":3000", nil))
}
